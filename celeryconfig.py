import settings

CELERY_IMPORTS = ("tasks",)

CELERY_TASK_SERIALIZER = 'pickle'

BROKER_BACKEND = 'mongodb'

CELERY_RESULT_BACKEND = 'mongodb'

CELERY_MONGODB_BACKEND_SETTINGS = {
    'host': settings.MONGO['host'],
    'database': settings.MONGO['db'],
    'username': settings.MONGO['username'],
    'password': settings.MONGO['password']
}
