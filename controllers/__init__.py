import logging
import cPickle

from tornado.web import RequestHandler
from models import User


class BaseHandler(RequestHandler):

    def __init__(self, *args, **kwargs):
        super(BaseHandler, self).__init__(*args, **kwargs)

    def get_current_user(self):
        data_user_cookie = self.get_secure_cookie('twitter_user')
        if not data_user_cookie:
            return None
        data_user_cookie = cPickle.loads(data_user_cookie)
        email = data_user_cookie.get('email')
        try:
            return User.objects(email=email)[0]
        except Exception as exc:
            logging.error(exc)
            return None

    def render_string(self, template, **kwargs):
        kwargs.update({'handler': self})
        return self.settings.get('template_env')\
            .get_template(template).render(**kwargs)

    def render(self, template, **kwargs):
        self.finish(self.render_string(template, **kwargs))
