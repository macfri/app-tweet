import logging

from tornado.web import asynchronous
from tornado.auth import TwitterMixin

from models import User
from controllers import BaseHandler


class Auth(BaseHandler):

    def get(self):
        nick = self.get_argument('nick', None)
        password = self.get_argument('password', None)

        if not nick or not password:
            status_code = 1
            message = 'twitter auth failed'
            token = ''
        else:

            user = User.auth(nick, password)

            if not user:
                status_code = 1
                message = 'twitter auth failed'
                token = ''
            else:
                status_code = 0
                message = 'ok'
                token = user.access_token

        logging.info('status_code: %s' % status_code)
        self.set_header("Access-Control-Allow-Origin", "*")
        self.finish(dict(status_code=status_code,
            message=message, token=token))


class Favorite(BaseHandler, TwitterMixin):

    @asynchronous
    def get(self):

        id = self.get_argument('id', None)
        nick = self.get_argument('nick', None)
        password = self.get_argument('password', None)

        if not nick or not password:
            self.finish(dict(status_code=1))

        user = User.auth(nick, password)

        if not user:
            self.send_error(500)
            return
        else:
            self.twitter_request(
                "/favorites/create/%s" % id,
                post_args={'a': 'b'},
                access_token=user.access_token,
                callback=self.async_callback(self._on_post)
            )

    def _on_post(self, new_entry):
        self.set_header("Access-Control-Allow-Origin", "*")

        if not new_entry:
            self.write(dict(status_code=1))
            return
        self.finish(dict(status_code=0))


class DirectMesaage(BaseHandler, TwitterMixin):

    @asynchronous
    def get(self):
        screen_name = self.get_argument('screen_name', None)
        message = self.get_argument('message', None)
        nick = self.get_argument('nick', None)
        password = self.get_argument('password', None)

        user = User.auth(nick, password)

        if not user:
            self.send_error(500)
            return
        else:
            self.twitter_request(
                "/direct_messages/new",
                post_args={'screen_name': screen_name, 'text': message},
                access_token=user.access_token,
                callback=self.async_callback(self._on_post)
            )

    def _on_post(self, new_entry):
        self.set_header("Content-Type", "text/json")

        if not new_entry:
            self.write('not send message')
            return
        self.finish("send direct message!")


class Timeline(BaseHandler, TwitterMixin):

    @asynchronous
    def get(self):

        nick = self.get_argument('nick', None)
        password = self.get_argument('password', None)
        since_id = self.get_argument('since_id', None)
        count = self.get_argument('count', 5)
        send_data = {}
        get_args = {'count': count}

        self.set_header("Access-Control-Allow-Origin", "*")

        if since_id:
            get_args.update({'since_id': since_id})

        if not nick or not password:
            send_data['status_code'] = 4
            send_data['message'] = 'invalid parameters'
            self.finish(send_data)

        user = User.auth(nick, password)

        if not user:
            send_data['status_code'] = 1
            send_data['message'] = 'twitter auth failed'
            self.finish(send_data)
        else:
            try:
                self.twitter_request(
                    "/statuses/home_timeline",
                    access_token=user.access_token,
                    callback=self.async_callback(self._on_post),
                    **get_args
                )
            except Exception as exc:
                send_data['status_code'] = 2
                send_data['message'] = str(exc)
                self.finish(send_data)

    def _on_post(self, data):
        self.set_header("Content-Type", "text/json")
        send_data = {}

        if not data:
            send_data['status_code'] = 3
            send_data['message'] = 'not timeline user'
            self.finish(send_data)
            return

        data_json = []
        for twitt in data:
            data_json.append(dict(
                id_str=twitt.get('id_str'),
                id_int=twitt.get('id'),
                profile_image_url_https=twitt
                    .get('user').get('profile_image_url_https'),
                from_user_id=twitt.get('user').get('id_str'),
                from_user_name=twitt.get('user').get('name'),
                from_user=twitt.get('user').get('screen_name'),
                text=twitt.get('text'),
                created_at=twitt.get('created_at')
            ))

        send_data['status_code'] = 0
        send_data['data'] = data_json
        self.finish(send_data)


class Retwit(BaseHandler, TwitterMixin):

    @asynchronous
    def get(self):
        twitter_id = self.get_argument('id', None)
        nick = self.get_argument('nick', None)
        password = self.get_argument('password', None)

        user = User.auth(nick, password)

        if not user:
            self.send_error(500)
            return
        else:
            self.twitter_request(
                "/statuses/retweet/" + twitter_id,
                post_args={'a': 'b'},
                access_token=user.access_token,
                callback=self.async_callback(self._on_post)
            )

    def _on_post(self, new_entry):
        self.set_header("Access-Control-Allow-Origin", "*")

        if not new_entry:
            self.write(dict(status_code=1))
            return
        self.finish(dict(status_code=0))


class Twit(BaseHandler, TwitterMixin):

    @asynchronous
    def get(self):

        nick = self.get_argument('nick', None)
        password = self.get_argument('password', None)
        message = self.get_argument('message', None)
        reply_id = self.get_argument('reply_id', None)
        send_data = {}

        self.set_header("Access-Control-Allow-Origin", "*")

        if not nick or not password or not message:
            send_data['status_code'] = 4
            send_data['message'] = 'invalid parameters'
            self.finish(send_data)

        user = User.auth(nick, password)

        if not user:
            send_data['status_code'] = 1
            send_data['message'] = 'twitter auth failed'
            self.finish(send_data)
        else:
            post_args = {}
            post_args['status'] = message.encode('utf-8')

            if reply_id:
                post_args['in_reply_to_status_id'] = reply_id

            try:
                self.twitter_request(
                    "/statuses/update",
                    post_args=post_args,
                    access_token=user.access_token,
                    callback=self.async_callback(self._on_post)
                )
            except Exception as exc:
                send_data['status_code'] = 2
                send_data['message'] = str(exc)
                self.finish(send_data)

    def _on_post(self, new_entry):
        self.set_header("Access-Control-Allow-Origin", "*")

        send_data = {}
        if not new_entry:
            send_data['status_code'] = 3
            send_data['message'] = 'cant not post'
            self.finish(send_data)
            return

        send_data['status_code'] = 0
        send_data['message'] = 'send post'
        self.finish(send_data)
