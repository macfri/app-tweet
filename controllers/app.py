import logging
import cPickle
import hashlib
import datetime
import html2text

from tornado.web import asynchronous, authenticated
from tornado.auth import TwitterMixin

from models import User
from controllers import BaseHandler

from utils import Mail
#from tasks import send_email
from tasks import send_email2 as send_email


class Index(BaseHandler):

    def get(self, **kwargs):

        user = self.get_current_user()

        if user:
            if user.user_id:
                self.redirect(self.reverse_url('twitter_profile'))
            else:
                self.redirect(self.reverse_url('twitter_add_account'))
            return
        else:

            status_code = None

            if 'status_code' in kwargs:
                status_code = kwargs['status_code']

        self.render('site/index.jade', status_code=status_code)

    def post(self):

        status_code = 0
        email = self.get_argument('email', None)
        nick = self.get_argument('nick', None)
        password = self.get_argument('password', None)
        confirm_password = self.get_argument('confirm_password', None)

        logging.info('nick: %s' % nick)
        logging.info('password: %s' % password)
        logging.info('email: %s' % email)
        logging.info('confirm_password: %s' % confirm_password)

        if not nick or not password or not email or not confirm_password:
            status_code |= 1
            logging.error('fill data')
        else:

            if password != confirm_password:
                status_code |= 2
                logging.error('password is diferent confirm_password')
            else:

                try:
                    password = hashlib.sha1(
                        password.encode('utf-8')).hexdigest()
                except Exception as exc:
                    logging.error(exc)
                    status_code |= 1

        if User.objects(email=email).first():
            status_code |= 16
        else:
            user = User.objects(
                nick=nick,
            ).first()

            if not user:
                logging.info('no exists database')
                user = User()
                user.nick = nick
                user.password = password
                user.email = email
                user.activation_passwd = User.get_token()

                try:
                    user.save()
                except Exception as exc:
                    logging.error(exc)
                    status_code |= 4
                else:
                    logging.info('added user!')
            else:
                status_code |= 8

        logging.info('status_code: %s' % status_code)

        if status_code == 0:
            data = cPickle.dumps({
                'nick': nick,
                'password': password,
                'email': email
            }, -1)

            self.set_secure_cookie("twitter_user", str(data))
            logging.info('set cookie user')
            self.redirect(self.reverse_url('twitter_add_account'))
            return

        self.get(status_code=status_code)


class Login(BaseHandler):

    def get(self, **kwargs):

        status_code = None
        if 'status_code' in kwargs:
            status_code = kwargs['status_code']
        self.render('site/login.jade', status_code=status_code)

    def post(self):

        status_code = 0
        nick = self.get_argument('nick', None)
        password = self.get_argument('password', None)

        logging.info('nick: %s' % nick)
        logging.info('password: %s' % password)

        if not nick or not password:
            status_code |= 1
        else:

            try:
                password = hashlib.sha1(
                    password.encode('utf-8')).hexdigest()
            except Exception as exc:
                logging.error(exc)
                status_code |= 1
            else:

                try:
                    user = User.objects(nick=nick,
                        password=password).first()
                except Exception as exc:
                    logging.error(exc)
                    status_code |= 1
                else:
                    if not user:
                        status_code |= 1
                    else:

                        logging.info('login')
                        user.last_login = datetime.datetime.now()

                        try:
                            user.save()
                        except Exception as exc:
                            logging.error(exc)
                        else:
                            logging.info('uodate last_login')

                        data = cPickle.dumps({
                            'nick': user.nick,
                            'email': user.email
                        }, -1)

                        self.set_secure_cookie("twitter_user", str(data))
                        logging.info('set cookie user')

                        if user.user_id:
                            self.redirect(
                                self.reverse_url('twitter_profile'))
                        else:
                            self.redirect(
                                self.reverse_url('twitter_add_account'))
                        return

        self.get(status_code=status_code)


class AddAcount(BaseHandler):

    @authenticated
    def get(self):
        self.render('site/add_account.jade',
            user=self.get_current_user()
        )


class Twitter(BaseHandler, TwitterMixin):

    @authenticated
    @asynchronous
    def get(self):

        if self.get_argument("oauth_token", None):
            self.get_authenticated_user(self.async_callback(self._on_auth))
            return
        self.authorize_redirect()

    def _on_auth(self, data_user):

        status_code = 0

        if not data_user:
            status_code |= 1
            logging.error('no data_user')
        else:
            data_user = data_user.get('access_token')
            user = self.get_current_user()

            if not user.user_id:
                logging.info('not relationship twitter')
                logging.info('data_user: %s' % data_user)

                user.user_id = data_user.get('user_id')
                user.access_token = data_user
                user.status = True

                try:
                    user.save()
                except Exception as exc:
                    logging.error(exc)
                    status_code |= 1
                else:
                    logging.info('update user')
            else:
                logging.info('relationship twitter')

        logging.info('status_code: %s' % status_code)
        self.redirect('%s?status_code=%s' % (
            self.reverse_url('twitter_response'), status_code)
        )


class Profile(BaseHandler):

    @authenticated
    def get(self, **kwargs):

        status_code = None
        if 'status_code' in kwargs:
            status_code = kwargs['status_code']

        self.render('site/profile.jade',
            user=self.get_current_user(),
            status_code=status_code
        )

    @authenticated
    def post(self):

        status_code = 0
        user = self.get_current_user()
        user.status = False
        user.access_token = {}
        user.user_id = ''

        try:
            user.save()
        except Exception as exc:
            logging.error(exc)
            status_code |= 1
        else:
            logging.info('update user')

        self.get(status_code=status_code)


class TwitterResponse(BaseHandler):

    def get(self):

        status_code = None
        self.render('site/add_account_response.jade',
            status_code=status_code)


class ForgotPassword(BaseHandler):

    def get(self, **kwargs):

        status_code = None
        if 'status_code' in kwargs:
            status_code = kwargs['status_code']
        self.render('site/forgot_password.jade',
            status_code=status_code)

    def post(self):

        status_code = 0
        email = self.get_argument('email', None)

        if not email:
            status_code |= 1
        else:
            try:
                user = User.objects(email=email).first()
            except Exception as exc:
                logging.error(exc)
                status_code |= 2
            else:

                if not user:
                    status_code |= 2
                else:

                    mail = Mail(
                        subject='[SMARTTWEET] Reset password',
                        sender=self.settings['email_config'].get('from'),
                        to=[email],
                    )

                    content = self.render_string(
                        'mail/change_password.html',
                        settings=self.settings,
                        nick=user.nick,
                        url='%s%s' % (
                            self.settings.get('domain_url'),
                            self.reverse_url("reset_password",
                                user.activation_passwd)
                        )
                    )

                    content_plain = html2text.html2text(content)
                    mail.add_body(content_plain, 'plain')
                    mail.add_body(content, 'html')
                    #send_email.delay(mail)
                    send_email(mail)
                    logging.info('send mail')

        logging.info('status_code: %s' % status_code)
        self.get(status_code=status_code)


class ResetPassword(BaseHandler):

    def get(self, token, **kwargs):

        logging.info('token: %s' % token)
        status_code = None

        if 'status_code' in kwargs:
            status_code = kwargs['status_code']

        if not self.get_cookie('token_pwd', None):

            try:
                user = User.objects(activation_passwd=token)[0]
            except:
                self.send_error(404)
                return
            else:
                logging.info('user: %s' % user.activation_passwd)
                self.set_cookie('token_pwd', user.activation_passwd)

        self.render('site/reset_password.jade', status_code=status_code)

    def post(self, token, **kwargs):

        status_code = 0
        password = self.get_argument('password', '')
        confirm_password = self.get_argument('confirm_password', '')

        try:
            user = User.objects(activation_passwd=token).first()
        except:
            self.send_error(404)
            return

        if len(password) < 0 or len(confirm_password) < 0:
            status_code |= 1
        elif not (password == confirm_password):
            status_code |= 2

        if status_code == 0:

            try:
                password = hashlib.sha1(
                    password.encode('utf-8')).hexdigest()
            except Exception as exc:
                logging.error(exc)
                status_code |= 1
            else:

                user.password = password
                user.activation_passwd = User.get_token()

                try:
                    user.save()
                except Exception as exc:
                    logging.error(exc)
                    status_code |= 4
                else:
                    self.clear_cookie('token_pwd')

        self.get(token, status_code=status_code)


class ChangePassword(BaseHandler):

    @authenticated
    def get(self, **kwargs):

        status_code = None
        if 'status_code' in kwargs:
            status_code = kwargs['status_code']
        self.render('site/change_password.jade',
            status_code=status_code,
            user=self.get_current_user()
        )

    @authenticated
    def post(self):

        status_code = 0
        user = self.get_current_user()
        password = self.get_argument('password', None)
        confirm_password = self.get_argument('confirm_password', None)
        logging.info('password: %s' % password)
        logging.info('confirm_password: %s' % confirm_password)

        if not password or not confirm_password:
            status_code |= 1
        else:
            if password != confirm_password:
                status_code |= 2
            else:

                try:
                    password = hashlib.sha1(
                        password.encode('utf-8')).hexdigest()
                except Exception as exc:
                    logging.error(exc)
                    status_code |= 1
                else:

                    user.password = password

                    try:
                        user.save()
                    except Exception as exc:
                        logging.error(exc)
                        status_code |= 4
                    else:
                        logging.info('save user')

        logging.info('status_code: %s' % status_code)
        self.get(status_code=status_code)


class Logout(BaseHandler):

    def get(self):
        self.clear_cookie('twitter_user')
        self.redirect(self.reverse_url('twitter_app'))
