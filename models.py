import uuid
import datetime
import settings
import logging
import hashlib
import mongoengine as me


class User(me.Document):

    nick = me.StringField(required=True, unique=True)
    password = me.StringField(required=True)
    email = me.StringField(required=True)
    access_token = me.DictField()
    user_id = me.StringField()
    created_at = me.DateTimeField(default=datetime.datetime.now)
    updated_at = me.DateTimeField()
    status = me.BooleanField(default=False)
    last_login = me.DateTimeField()
    activation_passwd = me.StringField()

    meta = {
        'collection': 'users',
        'indexes': [
            'nick',
            'email'
        ]
    }

    @classmethod
    def auth(self, nick, password):

        try:
            password = hashlib.sha1(
                password.encode('utf-8')).hexdigest()
        except Exception as exc:
            logging.error(exc)
            return False
        else:
            user = User.objects(
                nick=nick,
                password=password
            ).first()

            return user

    @classmethod
    def get_token(self):

        while True:
            activation = ('%s%s' % (
                settings.COOKIE_SECRET,
                uuid.uuid4().hex))[::-1]
            try:
                if self.objects(activation_passwd=activation).count() == 0:
                    activation = activation
                    break
            except Exception as exc:
                logging.error(exc)
                break
        return activation
