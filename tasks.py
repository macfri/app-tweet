from celery.task import task
import settings
import smtplib
import logging


@task(name='send_email', max_retries=3, ignore_result=True, safe_str=True)
def send_email(mail):
    try:
        conn = smtplib.SMTP()
        conn.connect(
            settings.EMAIL_CONFIG["smtp_server"],
            settings.EMAIL_CONFIG["smtp_port"]
        )
        conn.ehlo()
        conn.starttls()
        conn.ehlo()

        conn.login(
            settings.EMAIL_CONFIG["smtp_username"],
            settings.EMAIL_CONFIG["smtp_password"]
        )

        conn.sendmail(
            settings.EMAIL_CONFIG["from"],
            mail.to + mail.cc + mail.bcc,
            mail.as_string()
        )
    except smtplib.SMTPException as exc:
        send_email.retry(exc=exc)


def send_email2(mail):
    try:
        conn = smtplib.SMTP()
        conn.connect(
            settings.EMAIL_CONFIG["smtp_server"],
            settings.EMAIL_CONFIG["smtp_port"]
        )
        conn.ehlo()
        conn.starttls()
        conn.ehlo()

        conn.login(
            settings.EMAIL_CONFIG["smtp_username"],
            settings.EMAIL_CONFIG["smtp_password"]
        )

        conn.sendmail(
            settings.EMAIL_CONFIG["from"],
            mail.to + mail.cc + mail.bcc,
            mail.as_string()
        )
    except smtplib.SMTPException as exc:
	logging.info(exc)
        #send_email.retry(exc=exc)
