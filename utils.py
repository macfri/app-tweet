# -*- coding: utf-8 -*-
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE


class Mail(object):

    def __init__(self, subject='', sender='', to=None, cc=None, bcc=None):
        """
        @to list
        @cc list
        @bcc list
        """
        def to_list(val):
            if isinstance(val, list):
                return val
            else:
                return list(val) if val else []

        self._subject = subject
        self._sender = sender
        self._to = to_list(to)
        self._cc = to_list(cc)
        self._bcc = to_list(bcc)
        self._body = []

    @property
    def subject(self):
        return self._subject

    @subject.setter
    def subject(self, value=""):
        self._subject = value

    @property
    def sender(self):
        return self._sender

    @sender.setter
    def sender(self, value=""):
        self._sender = value

    @property
    def to(self):
        return self._to

    @to.setter
    def to(self, value=[]):
        value = value or []
        if isinstance(value, str):
            value = [value]
        self._to = list(value)

    @property
    def cc(self):
        return self._cc

    @cc.setter
    def cc(self, value):
        value = value or []
        if isinstance(value, str):
            value = [value]
        self._cc = list(value)

    @property
    def bcc(self):
        return self._bcc

    @bcc.setter
    def bcc(self, value):
        value = value or []
        if isinstance(value, str):
            value = [value]
        self._bcc = list(value)

    def add_body(self, content, type="plain"):
        self._body.append(MIMEText(content.encode("utf-8"), type, "utf-8"))

    def as_string(self):
        _mail = MIMEMultipart("alternative")
        _mail["Subject"] = self._subject
        _mail["From"] = self._sender
        _mail["To"] = COMMASPACE.join(self._to)
        if len(self._cc) > 0:
            _mail["CC"] = COMMASPACE.join(self._cc)
        for b in self._body:
            _mail.attach(b)
        return _mail.as_string()
