require([
  'util.add_account'
],
function(add_account) {
  window.add_account = add_account;

  $(function() {
    add_account.init();
  });
});
