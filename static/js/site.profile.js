require([
  'util.add_account'
],
function(add_account) {
  window.add_account = add_account;

  var cookie = {
    cache: 0,
    get: function(name, force) {
      var c, i;

      if (this.cache !== 0 && !force) {
        return this.cache[name];
      }

      this.cache = {};
      c = document.cookie.split(';');

      for (i in c) {
        i = c[i].split('=');
        this.cache[i[0]] = i[1];
      }

      return this.cache[name];
    }
  };

  var twitter = {
    remove: function(e) {
      e.preventDefault();
      var el = $(e.currentTarget);
      var form = $('<form/>', {
        action: el.attr('href'),
        method: 'post'
      });

      form.append($('<input/>', {
        name: '_xsrf',
        value: cookie.get('_xsrf')
      }));

      form.submit();
    },
    init: function() {
      $('#remove_twitter_account').on('click', $.proxy(this, 'remove'));
    }
  };

  $(function() {
    twitter.init();
    add_account.init();

    if (location.search.indexOf('?status_code=0') != -1) {
      $('#profile').prepend('<div class="message">Twitter account added.</div>');
    }
  });
});
