require([
  'libs/jquery.form',
  'libs/jquery.validate'
],

function(form) {
  var forgotpassword = {
    init: function() {
      this.form = $('#forgotpassword_form');
      this.form.validate({
        ignoreTitle: true,
        rules: {
          email: {
            email: true,
            required: true
          }
        }
      });

      this.form
        .on('click', '.placeholder', $.proxy(form, 'placeHolder'))
        .on('focus blur', '.customplaceholder', $.proxy(form, 'customPlaceHolder'));
    }
  };

  $(function() {
    forgotpassword.init();
  });
});
