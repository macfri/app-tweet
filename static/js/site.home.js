require([
  'libs/jquery.form',
  'libs/jquery.validate'
],

function(form) {
  var register = {
    init: function() {
      this.form = $('#register_form');
      this.form.validate({
        ignoreTitle: true,
        rules: {
          email: {
            required: true,
            email: true
          },
          nick: {
            required: true
          },
          password: {
            required: true
          },
          confirm_password: {
            equalTo: '#password'
          }
        }
      });
      this.form
        .on('click', '.placeholder', $.proxy(form, 'placeHolder'))
        .on('focus blur', '.customplaceholder', $.proxy(form, 'customPlaceHolder'));
    }
  };

  var login = {
    init: function() {
      this.form = $('#login_form');
      this.form.validate({
        ignoreTitle: true,
        rules: {
          nick: {
            required: true
          },
          password: {
            required: true
          }
        }
      });
      this.form
        .on('click', '.placeholder', $.proxy(form, 'placeHolder'))
        .on('focus blur', '.customplaceholder', $.proxy(form, 'customPlaceHolder'));
    }
  };

  $(function() {
    register.init();
    login.init();
  });
});
