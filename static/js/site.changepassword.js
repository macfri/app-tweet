require([
  'libs/jquery.form',
  'libs/jquery.validate'
],

function(form) {
  var changepassword = {
    init: function() {
      this.form = $('#changepassword_form');
      this.form.validate({
        ignoreTitle: true,
        rules: {
          password: {
            required: true
          },

          confirm_password: {
            equalTo: '#password'
          }
        }
      });

      this.form
        .on('click', '.placeholder', $.proxy(form, 'placeHolder'))
        .on('focus blur', '.customplaceholder', $.proxy(form, 'customPlaceHolder'));
    }
  };

  $(function() {
    changepassword.init();
  });
});
