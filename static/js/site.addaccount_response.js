require([
],
function() {
  function getArguments() {
    var s, r, i;
    s = location.search.split('?').join('').split('&');
    r = {};

    for (i in s) {
      if (s[i]) {
        s[i] = s[i].split('=');
        r[s[i][0]] = s[i][1];
      }
    }

    return r;
  }

  var response = {
    init: function(status_code) {
      var self = this;
      status_code = parseInt(status_code, 10);
      self.parent = window.opener;

      self.parent.add_account.response(status_code);
      window.close();
    }
  };

  $(function() {
    response.init(getArguments().status_code);
  });
});
