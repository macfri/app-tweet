!function($) {
  $.fn.placeholder = function() {

    return this.each(function() {
      $(this).on({
        'focus': function() {
          var el = $(this);
          if (el.val() === el.attr('placeholder')) {
            el.val('');
          }
        },

        'blur': function() {
          var el = $(this);
          if (el.val() === '') {
            el.val(el.attr('placeholder'));
          }
        }
      }).each(function() {
        var el = $(this);
        if (el.val() === '') {
          el.val(el.attr('placeholder'));
        }
      });
    });
  };
}(jQuery);
