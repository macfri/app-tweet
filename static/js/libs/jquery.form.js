define(function() {
  return {
    placeHolder: function(e) {
      e.preventDefault();
      var el = $(e.currentTarget);

      el.hide().parents('form')
        .find('#' + el.attr('for')).focus()
    },
    customPlaceHolder: function(e) {
      var el = $(e.currentTarget);

      if (e.type === 'focusin') {
        if (el.val() === '') {
          el.parent().find('.placeholder').hide();
        }
      }

      else if (e.type === 'focusout') {
        if (el.val() === '' || el.val() === el.attr('title')) {
          el.val('');
          el.parent().find('.placeholder').show();
        }
      }
    }
  };
});
