define(function() {
  return {
    response: function(status_code) {
      status_code = parseInt(status_code, 10);

      if (status_code === 0) {
        location.href = '/profile';
      }

      else {
        this.el.find('#response').empty().html(
          $('#message').html().split('%s').join('Account failed')
        );
      }

    },

    launch: function(e) {
      e.preventDefault();
      window.open($(e.currentTarget).attr('href'), 'twitter', 'width=840px,height=635px');
      this.el.find('#response').empty();
    },

    init: function() {
      this.el = $('#addaccount');
      $('#add_account_button').on('click', $.proxy(this, 'launch'));
    }
  };
});
