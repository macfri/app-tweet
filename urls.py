from tornado.web import url

from controllers.app import *
from controllers.tv import *

routers = (
    #tv
    url('/auth', Auth),
    url('/twit', Twit),
    url('/retwit', Retwit),
    url('/direct_message', DirectMesaage),
    url('/timeline', Timeline),
    url('/favorite', Favorite),

    #app
    url('/twitter_handler', Twitter, name='twitter_handler'),
    url('/twitter_response', TwitterResponse, name='twitter_response'),
    url('/forgot_password', ForgotPassword, name='forgot_password'),
    url('/change_password', ChangePassword, name='change_password'),
    url(r'/reset_password/(.*)', ResetPassword, name='reset_password'),
    url('/add_account', AddAcount, name='twitter_add_account'),
    url('/profile', Profile, name='twitter_profile'),
    url('/login', Login, name='login'),
    url('/logout', Logout, name='logout'),
    url('/', Index, name='twitter_app')
)
