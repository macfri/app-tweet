var build = function() {
  var requirejs = require('requirejs');

    var config = {
      appDir: 'static/js',
      baseUrl: '../build',
      dir: '../build',
      optimize: 'uglify',
      uglify: {
        toplevel: true
      },
      modules: [
        { name: '../build/site.addaccount.js' },
        { name: '../build/site.addaccount_response.js' },
        { name: '../build/site.changepassword.js' },
        { name: '../build/site.forgotpassword.js' },
        { name: '../build/site.home.js' },
        { name: '../build/site.profile.js' }
      ]
    };

    requirejs.optimize(config, function() {
      console.log('Revise ', config.dir);
    });
};

!function() {
  build();
}.call(this);
