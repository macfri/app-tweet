# -*- coding: utf-8 -*-
import os
import filters
from jinja2 import Environment, FileSystemLoader

DEBUG = False
XSRF_COOKIES = False
DOMAIN_URL = 'http://127.0.0.1:8888'
COOKIE_SECRET = 'msd44543444'
LOGIN_URL = '/'

_local_path = os.path.dirname(__file__)
STATIC_PATH = os.path.join(_local_path, 'static')
STATIC_URL_PREFIX = '/static/'

TEMPLATE_PATH = os.path.join(_local_path, 'templates')
TEMPLATE_ENV = Environment(loader=FileSystemLoader(TEMPLATE_PATH),
    extensions=['pyjade.ext.jinja.PyJadeExtension'])
TEMPLATE_ENV.filters['bitwise_flag'] = filters.bitwise_flag

TWITTER_CONSUMER_KEY = 'UyBx5NVpGMiQJgh0jU1Kug'
TWITTER_CONSUMER_SECRET = 'g32evW2ytcz70iUFoYQlqbMu93WabJCQvCMlFs3Pb9U'

MONGO = {
    'db': 'smart_twit',
    'username': None,
    'password': None,
    'host': '127.0.0.1',
    'port': 27017
}

EMAIL_CONFIG = {
    'smtp_server': 'smtp.gmail.com',
    'smtp_port': 587,
    'smtp_password': 'm4cf1_123',
    'smtp_username': 'm4cf1.server',
    'from': 'm4cf1_server@gmail.com',
}

try:
    from local_settings import *
except ImportError:
    pass
